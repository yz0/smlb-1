# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 22:26:37 2021

@author: mdzha
"""

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

car_data = pd.read_csv("trainingData.csv")
# print(car_data)

def replace_cartype(cartype):
    try:
        types1 = ['van', 'regcar', 'truck', 'sportuv', 'sportcar', 'stwagon']
        return types1.index(cartype)
    except ValueError:
        return cartype
    
def replace_fueltype(fueltype):
    try:
        fules = ['cng', 'methanol', 'electric', 'gasoline']
        return fules.index(fueltype)
    except ValueError:
        return fueltype

def format_data(data):
    for name in data.columns[5:11]:
        data[name] = data[name].apply(replace_cartype)
    # print(data.columns[5:11])
    
    for fuel in data.columns[11:17]:
        data[fuel] = data[fuel].apply(replace_fueltype)
    # print(data.columns[11:17])
    
    return data

car_data = format_data(car_data)
# car_data.head()
# car_data.isnull().sum()

columns_to_drop = ['id']
car_data = car_data.drop(columns_to_drop,axis='columns')
# print(car_data.columns)
# print(f'car_data: {car_data.shape}')

X_train, X_test, y_train, y_test = train_test_split(
    car_data.drop(['choice'], axis=1),
    car_data.choice,
    test_size = 0.10,
    train_size = 0.90)

print(f'X_train: {X_train.shape}')
print(f'y_train: {y_train.shape}')
print(f'X_test: {X_test.shape}')
print(f'y_test: {y_test.shape}')

import numpy as np
n_estimators = [int(x) for x in np.linspace(start = 10, stop = 100, num = 10)]
max_features = ['auto', 'sqrt']
max_depth = [10, 11, 12, 13, 14, 15]
min_samples_split = [int(x) for x in np.linspace(start = 30, stop = 40, num = 5)]
min_samples_leaf = [int(x) for x in np.linspace(start = 30, stop = 40, num = 5)]
bootstrap = [True, False]

param_grid = {
    'n_estimators': n_estimators,
    'max_features': max_features,
    'max_depth': max_depth,
    'min_samples_split': min_samples_split,
    'min_samples_leaf': min_samples_leaf,
    'bootstrap': bootstrap,
    }

model = RandomForestClassifier()

from sklearn.model_selection import GridSearchCV
grid = GridSearchCV(estimator = model, param_grid = param_grid, cv = 3, verbose = 2, n_jobs = -1)

print('Optimization may take 20+ minutes ...')
grid.fit(X_train, y_train)

print('\n\n\n#')
print(f'# {grid.best_params_}')
print(f'# Train Accuracy - : {grid.score(X_train, y_train):.3f} ; Test Accuracy - : {grid.score(X_test, y_test):.3f}')
print(f'# param_grid = {param_grid}')

best_params = grid.best_params_

model = RandomForestClassifier(
            bootstrap = best_params['bootstrap'],
            max_depth = best_params['max_depth'],
            max_features = best_params['max_features'],
            min_samples_leaf = best_params['min_samples_leaf'],
            min_samples_split = best_params['min_samples_split'],
            n_estimators = best_params['n_estimators'],
            )

model.fit(X_train, y_train)

model_score = model.score(X_test, y_test)
print(model_score)

def get_results(test_data):
    test_data = format_data(test_data)
    results = model.predict(test_data.drop(['choice'], axis=1))
    results = results.tolist()
    id_list = [f"id{i}" for i in range(1, 1 + len(results))]
    prediction = list(zip(id_list, results))
    return prediction

#
# print(get_results(car_data))

