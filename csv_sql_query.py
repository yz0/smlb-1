# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 00:40:48 2021

@author: mdzha
"""

import pandas as pd
import sqlite3

#https://sqlitebrowser.org/blog/portableapp-for-3-12-0-release-now-available/

#read the CSV

df = pd.read_csv('trainingData.csv')
#connect to a database
conn = sqlite3.connect("trainingData.db") #if the db does not exist, this creates a Any_Database_Name.db file in the current directory
#store your table in the database:
df.to_sql('Some_Table_Name', conn, if_exists='replace')

#read a SQL Query out of your database and into a pandas dataframe
sql_string = """

select distinct
      fuel1 as fuel
    , station1 as station
from some_table_name
union
select distinct
      fuel2 as fuel
    , station2 as station
from some_table_name
union
select distinct
      fuel3 as fuel
    , station3 as station
from some_table_name
union
select distinct
      fuel4 as fuel
    , station4 as station
from some_table_name
union
select distinct
      fuel5 as fuel
    , station5 as station
from some_table_name
union
select distinct
      fuel6 as fuel
    , station6 as station
from some_table_name


"""
df = pd.read_sql(sql_string, conn)
print(df)
