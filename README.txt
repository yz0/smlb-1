List of files:
1. ml.py:
   machien learning model for car sales prediction, with its hyperparameters
   tuned up to roughly 40% accuracy.
   
2. ml_optimize.py:
   the model including the optimization process using GridSearchCV,
   the "grid" is an estimated range of parameters, and the "fit" process will
   find out the optimum configuration. The script will automatically build a
   classifier with the parameters and carry out the rest of the process.
   Notice that the optimization takes 20 minutes on a computer with i7 CPU.
   
Misc:
3. csv_sql_query.py:
   an example of querying .csv file with SQL, as we chatted in meeting.
   
4. conda_list.txt:
   a list of python libraries used in my system.
   
How to run:
   Place trainingData.csv in the same directory of the .py files, and 
   notice the working directory.
   
   Example console command:
   runfile('C:/smlb_1/ml.py', wdir='C:/smlb_1/')
   
Repository on GitLab.com:
   https://gitlab.com/yz0/smlb-1/-/tree/master/test
