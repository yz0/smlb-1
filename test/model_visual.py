# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 02:05:10 2021

@author: mdzha
"""

def visualize_model(mdoel):
    img_path = 'tree/'

    # Remove existing .dot and .png files in img_path    
    import os
    filelist = [ f for f in os.listdir(img_path) if
                        (f.endswith('.png') or f.endswith('.dot')) ]
    for f in filelist:
        os.remove(os.path.join(img_path, f))

    for i in range(0, len(model.estimators_)):
        estimator = model.estimators_[i]
        tree = img_path + '/' + 'tree_'+str(i)
        from sklearn.tree import export_graphviz
        # Export as dot file
        export_graphviz(estimator, out_file=tree+'.dot', 
                        feature_names = iris.feature_names,
                        class_names = iris.target_names,
                        rounded = True, proportion = False, 
                        precision = 2, filled = True)
        
        # Convert to png
        import pydot
        (graph,) = pydot.graph_from_dot_file(tree+'.dot')
        graph.write_png(tree+'.png')

        # # Display in jupyter notebook
        # from IPython.display import Image
        # Image(filename = 'tree.png')


# For test
from sklearn.datasets import load_iris
iris = load_iris()

# Model (can also use single decision tree)
from sklearn.ensemble import RandomForestClassifier
model = RandomForestClassifier(n_estimators=10)

# Train
model.fit(iris.data, iris.target)
# Extract single tree
    
visualize_model(model)

